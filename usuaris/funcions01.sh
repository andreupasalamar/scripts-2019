#! /bin/bash
# @ edt ASIX M01-ISO
# Curs 2019 - 2020
# funcions
# -----------------

# showUser(login)
# mostrar un a un els camps amb label
function showUser(){
  # Validar arguments
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrectes"
    echo "USAGE: showUser login"
    return 1
  fi
  # Validar existencia
  grep "^$1:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: usuari $1 no existeix"
    echo "USAGE: showUser login"
    return 2
  fi
  
  # Main
  user=$1
  line=$(grep "^$user:" /etc/passwd 2>/dev/null)
  uid=$(echo "$line" | cut -d: -f3)
  gid=$(echo "$line" | cut -d: -f4)
  gecos=$(echo "$line" | cut -d: -f5)
  home=$(echo "$line" | cut -d: -f6)
  sh=$(echo "$line" | cut -d: -f7)
  gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

  echo "User: $user"
  echo "Uid: $uid"
  echo "Gid: $gid"
  echo "Gecos: $gecos"
  echo "Home: $home"
  echo "Bash: $sh"
  echo "Gname: $gname"
  return 0
}

function showUserList(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  OK=0
  # Validar arguments
  if [ $# -lt 1 ]; then
    echo "ERROR: num args incorrectes"
    echo "USAGE: showUserList login[...]"
    return $ERR_NARGS
  fi
  
  # Main
  error=$OK
  llistat=$*
  for user in $llistat; do
    # Validar existencia
    line=$(grep "^$user:" /etc/passwd 2> /dev/null)
    if [ -z $line ]; then
      echo "ERROR: usuari $user no existeix" >> /dev/stderr
      echo "USAGE: showUser login" >> /dev/stderr
      error=$ERR_NOLOGIN
    else
    uid=$(echo "$line" | cut -d: -f3)
    gid=$(echo "$line" | cut -d: -f4)
    gecos=$(echo "$line" | cut -d: -f5)
    home=$(echo "$line" | cut -d: -f6)
    sh=$(echo "$line" | cut -d: -f7)
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

    echo "User: $user"
    echo "Uid: $uid"
    echo "Gid: $gid"
    echo "Gecos: $gecos"
    echo "Home: $home"
    echo "Bash: $sh"
    echo "Gname: $gname"
    echo
    fi
  done
  return $error
}

function showUserIn(){
  ERR_NOLOGIN=2
  OK=0
  error=$OK
  while read -r user; do
    # Validar existencia
    grep "^$user:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "ERROR: usuari $1 no existeix" >> /dev/stderr
      echo "USAGE: showUser login" >> /dev/stderr
      error=$ERR_NOLOGIN
    else
      line=$(grep "^$user:" /etc/passwd 2>/dev/null)
      uid=$(echo "$line" | cut -d: -f3)
      gid=$(echo "$line" | cut -d: -f4)
      gecos=$(echo "$line" | cut -d: -f5)
      home=$(echo "$line" | cut -d: -f6)
      sh=$(echo "$line" | cut -d: -f7)
      gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

      echo "User: $user"
      echo "Uid: $uid"
      echo "Gid: $gid"
      echo "Gecos: $gecos"
      echo "Home: $home"
      echo "Bash: $sh"
      echo "Gname: $gname"
      echo
    fi
  done
  return $error
}


function showUserGecos(){
  # Vialidar arguments
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrectes"
    echo "USAGE: showUserGecos login"
    return 1
  fi
  # Validar existencia
  grep "^$1:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: usuari $1 no existeix"
    echo "USAGE: showUserGecos login"
    return 2
  fi
  # Main
  user=$1
  line=$(grep "^$user:" /etc/passwd 2>/dev/null)
  gecos=$(echo "$line" | cut -d: -f5)
  fname=$(echo "$gecos" | cut -d, -f1)
  office=$(echo "$gecos" | cut -d, -f2)
  ophone=$(echo "$gecos" | cut -d, -f3)
  hphone=$(echo "$gecos" | cut -d, -f4)

  echo "Nom complet: $fname"
  echo "Oficina: $office"
  echo "Telèfon del treball: $ophone"
  echo "Telèfon domicili: $hphone"
  echo

  return 0
}

function showGroupMainMenmbers(){
  # Validar arguments
  if [ $# -ne 1 ]; then
    echo "ERROR: nombre d'arguments incorrecte"
    echo "USAGE: showGroupMainMembers gname"
    return 1
  fi

  # Validar existencia
  line=$(grep "^$1:" /etc/group 2> /dev/null)
  if [ -z $line ]; then
    echo "ERROR: el grup $1 no existeix"
    echo "USAGE: showGroupMainMembers gname"
    return 2
  fi

  #Main
  gname=$1
  gid=$(echo "$line" | cut -d: -f3)
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 | sort -t' ' -k2g,2 | tr '[:lower:]' '[:upper:]' | sed 's/:/  /g' | sed -r 's/^(.*)$/\t\1/g'
  echo
  return 0
}

function showAllShells(){
  maxUsers=2
  llistaShells=$(cut -d: -f7 /etc/passwd | sort -u)
  for shell in $llistaShells; do
    users=$(grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort | sed -r 's/^(.*)$/\t\1/g')
    count=$(echo "$users" | wc -l)
    if [ $count -gt $maxUsers ]; then
      echo "$shell ($count)"
      echo "$users"
    fi
  done
  return 0
}

function showGroup(){
  # Validar arguments
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrectes"
    echo "USAGE: showGroup group"
    return 1
  fi
  
  # Validar existencia
  grep "^$1:" /etc/group &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: el group $1 no existeix"
    echo "USAGE: showGroup group"
    return 2
  fi

  #Main
  gname=$1
  line=$(grep "^$gname:" /etc/group 2>/dev/null)
  gid=$(echo "$line" | cut -d: -f3)
  gusers=$(echo "$line" | cut -d: -f4)

  echo "Nom del grup: $gname"
  echo "GID: $gid"
  echo "Usuaris: $gusers"
  echo 
  return 0
}

function showShadow(){
  #Declarar ERR
  ERR_NARG=1
  ERR_NLOGIN=2
  OK=0
  #Valida args
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: showShadow login"
    return $ERR_NARG
  fi
  login=$1
  liniaLogin=$(grep "^$login:" shadow.txt 2> /dev/null)
  #Valida login
  if [ -z "$liniaLogin" ]; then
    echo "ERROR: el login $1 no existeix o accés denegat."
    echo "USAGE: showShadow login"
    return $ERR_NLOGIN
  fi
  #Main
  user=$(echo "$liniaLogin" | cut -d: -f1)
  password=$(echo "$liniaLogin" | cut -d: -f2)
  dataCanvi=$(echo "$liniaLogin" | cut -d: -f3)
  minAge=$(echo"$liniaLogin" | cut -d: -f4)
  maxAge=$(echo "$liniaLogin" | cut -d: -f5)
  diesWarning=$(echo "$liniaLogin" | cut -d: -f6)
  diesInactiv=$(echo "$liniaLogin" | cut -d: -f7)
  dataExpir=$(echo "$liniaLogin" | cut -d: -f8)
  echo "Login: $user"
  echo "Contrasenya: $password"
  echo "Últim canvi: $dataCanvi"
  echo "Temps mínim: $minAge"
  echo "Temps màxim: $maxAge"
  echo "Dies fins expiració: $diesWarning"
  echo "Dies expirat: $diesInactiv"
  echo "Data expiració: $dataExpir"
  return $OK
}

function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}
