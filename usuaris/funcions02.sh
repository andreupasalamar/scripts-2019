#! /bin/bash
# @ edt ASIX M01-ISO
# Curs 2019 - 2020
# funcions
# -----------------

function numStdin(){
  counter=1
  while read -r line; do
    echo "$counter: $line"
    ((counter++))
  done
  return 0
}

function numFile(){
  ERR_ARGS=1
  ERR_NOFILE=2
	
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: numFile file"
    return $ERR_ARGS
  fi

  if ! [ -f $1 ]; then
    echo "ERROR: l'arxiu $1 no existeix"
    echo "USAGE: numFile file"
    return $ERR_NOFILE
  fi

  counter=1
  fileIn=$1
  while read -r line; do
    echo "$counter: $line"
    ((counter++))
  done < $fileIn
  return 0
}

function numFileDefault(){
  if [ $# -gt 1 ];then
    echo "ERROR: num args incorrecte"
    echo "USAGE: numFileDefault"
    return 1
  fi
  
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    if [ ! -f "$1" ]; then
      echo "ERROR: $1 no es un fitxer"
      echo"USAGE:"
      return 2
    fi
  fileIn=$1
  fi
  cont=1
  while read -r line; do
    echo "$cont: $line"
    ((cont++))
  done < $fileIn
  return 0
}

function filterGid(){
  if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: filter Gid arxiu"
    return 1
  fi

  if ! [ -f $1 ]; then
    echo "ERROR: $1 no es un regular file"
    echo "USAGE: filterGid arxiu"
    return 2
  fi

  fileIn=$1

  while read -r line; do
    gid=$(echo "$line" | cut -d: -f4)
    if [ $gid -ge 500 ]; then
      echo "$line" | cut -d: -f1,3,4,6
    fi
  done < $fileIn
  return 0
}

filterGidDefault(){
  
 if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: filter Gid arxiu"
    return 1
  fi

  if ! [ -f $1 ]; then
    echo "ERROR: $1 no es un regular file"
    echo "USAGE: filterGid arxiu"
    return 2
  fi

  fileIn=$1

  while read -r line; do
    gid=$(echo "$line" | cut -d: -f4)
    if [ $gid -ge 500 ]; then
      echo "$line" | cut -d: -f1,3,4,6
    fi
  done < $fileIn
  return 0
  
  
}

