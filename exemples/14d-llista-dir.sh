#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 10 Febrer 2020
# dir si es un fitxer regular, un link o una altra cosa per cada element
# prorama dir[...]
# -----------------------------------------------

# Validar arguments
if [ $# -lt 1 ]; then	  
  echo "Error: nombre d'arguments incorrecte. Ha de ser un o més"
  echo "Usage: programa dir[...]"
  exit 1
fi


for directori in $*; do

  #Validar si és directori + Xixa
  if ! [ -d $directori ]; then
    echo "Error: l'argument no és un directori."
    echo "Usage: programa dir"
  else

    llistat=$(ls $directori)
    num=1
    for file in $llistat; do
      if [ -h $directori/$file ]; then
        tipuis="link"
      elif [ -d $directori/$file ]; then
        tipus="directori"
      elif [ -f $directori/$file ]; then
        tipus="regular"
      else
        tipus="altres"
      fi

      echo $num: $file $tipus
  
      num=$((num+1))
    done
  fi
  echo
done
exit 0
      
