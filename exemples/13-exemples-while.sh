#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 5 Febrer 2020
# Exemples dels bucles while
#---------------------------
#
while [ $# -gt 0 ]
do
  echo "$#: $*"
  shift
done
exit 0
# Decrement del arg a 0
numero=$1
while [ $numero -ge 0 ]
do
  echo $numero
  numero=$((numero-1))
done
exit 0
#del 1 al 10
numero=1
while [ $numero -le 10 ]
do
  echo $numero
  numero=$((numero+1))
done
exit 0
