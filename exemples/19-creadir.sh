#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 18 Febrer 2020
# Programa que realitza una sortida que digui quines son les opcions i quins els arguments
#	programa noudir[...]
# -----------------------------------------------
ERR_NARGS=1
ERR_MKDIR=2
#Validar arguments
if ! [ $# -ge 1 ]; then
  echo "Error: nombre arguments invàlid. Ha de ser 1 o més"
  echo "Usage: $0 noudir[...]"
  exit $ERR_NARGS
fi
#Xixa
llista=$*
numeroexit=0
for dir in $llista; do
  mkdir $dir &> /dev/null
  if [ $? -ne 0 ]; then
    echo "Error: el directori $dir no s'ha pogut crear. Passant al següent..." >> /dev/stderr
    numeroexit=$ERR_MKDIR
  fi
done
exit $numeroexit

