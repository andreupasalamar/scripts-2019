#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 10 Febrer 2020
# 
# prorama dir
# -----------------------------------------------

# Validar arguments
if [ $# -ne 1 ]; then	  
  echo "Error: nombre d'arguments incorrecte. Ha de ser un"
  echo "Usage: programa dir"
  exit 1
fi

#Validar si és directori
if ! [ -d $1 ]; then
  echo "Error: l'argument no és un directori."
  echo "Usage: programa dir"
  exit 2
fi

#Xixa

dir=$1
llistat=$(ls $dir)
num=1
for file in $llistat; do
  echo $num: $file
  num=$((num+1))
done

exit 0
      
