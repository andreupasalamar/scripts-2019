#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 5 Febrer 2020
# Per un conjunt de notes mostrar la qualificació
#	programa nota[...]
#------------------------------------------------
#Validar nombre arguments
if [ $# -lt 1 ]; then
  echo "Error: nombre d'arguments menor que un. Han de ser un o més."
  echo "Usage: $0 nota[...]"
  exit 1
fi
#Xixa
lnotes=$*
for nota in $lnotes
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
    echo "Error: la nota $nota no entra dins del rang" >> /dev/stderr 
    echo "El rang és entre 0 i 10." >> /dev/stderr
  elif [ $nota -lt 5 ]; then
    echo "$nota : suspès"
  elif [ $nota -lt 7 ]; then
    echo "$nota : aprovat"
  elif [ $nota -lt 9 ]; then
    echo "$nota : notable"
  else
    echo "$nota : excel·lent"
  fi
done
exit 0
	      
