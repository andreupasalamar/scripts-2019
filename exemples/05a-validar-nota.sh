#! /usr/bin/bash
# isx20612296@edt
# Andreu Pasalamar Carbó
# 3 Febrer 2020
# Programa que valida la nota
#       $ prog nota
#----------------------------
# Validar argument
if [ $# -ne 1 ]; then
  echo "Error: has introduït $# arguments"
  echo "Usage: $0 nota"
  exit 1
fi

if ! [ $1 -ge 0 -a $1 -le 10 ]; then
  echo "Error: nota $1 no vàlida [0-10]"
  echo "Usage: $0 nota"
  exit 2
fi

#main

nota=$1
if [ $nota -lt 5 ]; then
  echo "Suspès"	
else
  echo "Aprovat"
fi

