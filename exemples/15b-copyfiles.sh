#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 11 Febrer 2020
# exercicis copy
# prorama arxiu directori
# -----------------------------------------------
#Validar arxiu
#Validar nombre arguments
if ! [ $# -gt 1 ]; then
  echo "Error: Nombre d'arguments incorrecte. Han de ser dos o més"
  echo "Usage: $0 file[...] dir-desti"
  exit 1
fi

#desti=$(echo $* | sed 's/^.* //')
#llistatFile=$(echo $* | sed 's/ [^ ]*$//')
desti=$(echo $* | cut -d' ' -f$#)
llistatFile=$(echo $* | cut -d' ' -f1-$(($#-1)))
if ! [ -d $desti ]; then
  echo "Error: $desti no és un directori"
  echo "usage: $0 file[...] dir-desti"
  exit 2
fi

#Xixa

for file in $llistatFile; do
  if ! [ -f $file]; then
    echo "Error: $file no és un regular file" >>  /dev/stderr
    echo "usage: $0 file[..] dir-desti" >> /dev/stderr
  fi
  cp $file $desti
done
exit 0
