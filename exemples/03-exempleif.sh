#! /usr/bin
# isx20612296@edt
# Andreu Pasalamar Carbó
# 29 Gener 2020
# Exemple if
#	$ prog edat
# -------------------------
# validar argument
if [ $# -ne 1 ]; then
	echo "Error: has introduït $# arguments"
	echo "Usage: $0 edat"
	exit 1
fi
#xixa
edat=$1
if [ $edat -ge 18 ]; then
	echo "edat $edat es major d'edat"
fi
exit 0

