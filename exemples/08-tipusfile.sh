# isx20612296@edt
# Andreu Pasalamar Carbó
# 4 Febrer 2020
# Dir quin tipus de arg es
#       $ prog arg
#----------------------------
#Validar nombre arguments
if [ $# -ne 1 ]; then
  echo "ERROR:Nombre d'arguments invàlid. Ha de ser 1"
  echo "USAGE:programa argument"
  exit 1
fi
#Validar argument vàlid
if ! [ -e $1 ]; then
  echo "ERROR: L'argument no existeix. Ha d'existir perquè el programa funcioni"
  echo "USAGE: programa argument"
  exit 2
fi
#Xixa
file=$1
if [ -L $file ]; then
  echo "$file és un symbolic link"
elif [ -d $file ]; then
  echo "$file és un directori"
elif [ -f $file ]; then
  echo "$file és un regular file"
else
  echo "$file no és ni regular file, ni directori ni symbolic link"
fi
exit 0

