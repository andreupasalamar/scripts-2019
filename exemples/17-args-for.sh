#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 17 Febrer 2020
# Programa que realitza una sortida que digui quines son les opcions i quins els arguments
#	programa args[-a -b -c -d -e -f]
# -----------------------------------------------
ERR_NARGS=1

#Validar arguments
if ! [ $# -ge 1 ]; then
  echo "Error: nombre d'arguments invàlid. Han de ser 1 ó més."
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi

#Xixa
arguments=""
opcions=""
for arg in $*;do
  case $arg in
  "-a" | "-b" | "-c" | "-d" | "-e" | "-f")
    opcions="$opcions $arg";;
  *)
    arguments="$arguments $arg";;
  esac
done

echo "Arguments: $arguments"
echo "Opcions: $opcions"
exit 0

