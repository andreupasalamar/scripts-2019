#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 17 Febrer 2020
# Programa que realitza una sortida que digui quines son les opcions i quins els arguments
#	programa -a file -b -c -d -edat -e ... arg[...]
# -----------------------------------------------
ERR_NARGS=1

#Validar arguments
if ! [ $# -ge 1 ]; then
  echo "Error: nombre d'arguments invàlid. Han de ser 1 ó més."
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi

#Xixa
opcions=""
arguments=""
while [ -n "$1" ];do
  case $1 in
  "-a")
    file=$2
    shift;;
  "-d")
    edat=$2
    shift;;
  "-b" | "-c" | "-e" | "-f")
    opcions="$opcions $1";;
  *)
    arguments="$arguments $1";;
  esac
  shift
done

echo "file: $file"
echo "edat: $edat"
echo "Arguments: $arguments"
echo "Opcions: $opcions"
exit 0

