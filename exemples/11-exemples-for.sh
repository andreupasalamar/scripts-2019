#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 5 Febrer 2020
# Exemples del for
#------------------------
#numerades
count=1
for nom in $*
do
  echo $count : $nom
  count=$((count+1))
done
exit 0
#dollars @
for nom in "$@"
do
  echo $nom
done
exit 0
#iterar per la llista d'arguments
for nom in $*
do
 echo $nom
done
exit 0
llistat=$(ls)
for nom in $llistat
do
  echo $nom
done  
exit 0
#Iterar pel contingut d'una cadena
#Només itera una vegada
for nom in "pere pau marta anna"
do
  echo $nom
done
exit 0
#Iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo $nom
done
exit 0
