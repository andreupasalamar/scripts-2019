#! /usr/bin/bash
# isx20612296@edt
# Andreu Pasalamar Carbó
# 3 Febrer 2020
# Llista el directori indicat
#       $ prog directori
#----------------------------
# Validar argument
if [ $# -ne 1 ]; then
  echo "Error: has introduït $# arguments"
  echo "Usage: $0 directori"
  exit 1
fi

if ! [ -d $1 ]; then
  echo "Error: el directori $1 no es pot trobar"
  echo "Usage: $0 directori"
  exit 2
fi

#main
dir=$1
ls $dir
exit 0

