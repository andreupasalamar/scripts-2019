#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 10 Febrer 2020
# més exemples de while
#	programa arguments[...]
# -----------------------------------------------

# Programa que mostra tot el que li passem com a stdin en majuscules

num=1
while read -r line; do
  echo "$num: $(echo $line | tr [a-z] [A-Z])"
  num=$((num+1))
done
exit 0

# Processar entrada estàndard fins token de finalització FI

read -r line
while ! [ $line = "FI" ]; do
  echo $line
  read -r line
done
exit 0

# Mostrar l'entrada estàndard numerant les línies

count=1
while read -r line; do
  echo "$count: $line"
  count=$((count+1))
done

#6) Mostrar tot el que rep com a entrada estandar

while read -r line; do
  echo $line
done
exit 0

# contabilitzar tots els arguments

contador=1
while [ -n "$1" ]; do  
  echo "$contador: $1, $#, $*"
  contador=$((contador+1))
  shift
done
exit 0


