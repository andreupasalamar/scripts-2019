#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 10 Febrer 2020
# Validar que es rep un argument i que és un directori i llistar-ne el contingut
# Per llistar el contingut amb un simple ls ja n'hi ha prou
#	programa dir
# -----------------------------------------------

# Validar arguments
if [ $# -ne 1 ]; then	  
  echo "Error: nombre d'arguments incorrecte. Ha de ser un"
  echo "Usage: programa dir"
  exit 1
fi

#Validar si és directori
if ! [ -d $1 ]; then
  echo "Error: l'argument no és un directori."
  echo "Usage: programa dir"
  exit 1
fi

#Xixa

dir=$1

ls $dir

exit 0
      
