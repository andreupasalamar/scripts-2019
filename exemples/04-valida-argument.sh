#! /usr/bin/bash
# isx20612296@edt
# Andreu Pasalamar Carbó
# 3 Febrer 2020
# Programa que valida la synopsis i mostra els arguments
#       $ prog arg1 arg2
#-------------------------------------------------------
# Validar synopsis

if [ $# -ne 2 ]; then
	echo "Error: has introduït $# arguments"
	echo "Usage: $0 arg1 arg2"
	exit 1
fi
#main

echo "Els dos arguments son: $1, $2"
exit 0

