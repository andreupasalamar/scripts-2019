#! /bin/bash
# isx20612296@edt
# Andreu Pasalamar Carbó
# 4 Febrer 2020
# Validar si existeix dia del mes
#	prog mes
# ----------------------
#Ajuda
if [ $1 = "-h" ]; then
  echo "! /bin/bash"
  echo " isx20612296"
  echo " Andreu Pasalamar Carbó"
  echo " 4 Febrer 2020"
  echo " Aquest programa ens diu quin nombre de dies té el mes"
  exit 0
fi
# Validar argument
if [ $# -ne 1 ]; then
  echo "Error: has introduït $# arguments"
  echo "Usage: $0 dia mes"
  exit 1
fi
# Validar mes
if [ $1 -lt 1 -a $1 -gt 12 ]; then
  echo "Error: el mes no està entre 1 i 12"
  echo "Usage: $0 dia mes"
  exit 2
fi
#Xixa
mes=$1
case $mes in
  "2")
    echo "28 dies"
    ;;
  "4" | "6" | "9" | "11")
    echo "30 dies"
    ;;
  *)
    echo "31 dies"
    ;;
esac
exit 0

