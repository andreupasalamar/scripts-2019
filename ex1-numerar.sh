#! /bin/bash
# isx20612296
# Andreu Pasalamar Carbó
# 18 Febrer 2020
# Programa quemostra l'entrada estàndard numerada
# -----------------------------------------------

#Xixa
numero=1
while read -r line; do
  echo "$numero $line"
  numero=$((numero+1))
done
exit 0
